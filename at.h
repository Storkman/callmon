enum { AT_LINE_NONE, AT_LINE_OK, AT_LINE_ERROR, AT_LINE_RESP, AT_LINE_URC, AT_LINE_INVALID };
enum { AT_ARG_END, AT_ARG_NUM, AT_ARG_STR };
enum { AT_ERR_ERROR, AT_ERR_CME, AT_ERR_CMF };
enum {
	AT_URC_NONE,
	AT_URC_BUSY,
	AT_URC_NO_ANSWER,
	AT_URC_NO_CARRIER,
	AT_URC_NO_DIALTONE,
	AT_URC_EXTENDED,
	AT_URC_RING,
	AT_URC_UNKNOWN,
};

typedef struct ATConn_ {
	int fd;
	
	int cmdrunning;
	char cmd[32];
	
	int inplen, outlen, outpos, outechoed;
	char inp[512], out[256];
	
	char line[256];
	
	int error;
	const char *errstr;
} ATConn;

struct ATArg {
	int type;		/* AT_ARG_... */
	int num;		/* if AT_ARG_NUM */
	char *str;		/* if AT_ARG_STR */
};

struct ATCmd {
	char cmd[32];
	char args[256];
};

struct ATLine {
	int type;		/* AT_LINE_... */
	char *raw;
	
	char cmd[32];		/* command name if type is AT_LINE_RESP or AT_LINE_URC */
	int final;		/* true if this is a final response */
	int nargs;
	struct ATArg args[16];
	
	int errtype;		/* AT_ERR_... */
	int errnum;		/* CME/CMF error number */
	
	int urc;		/* AT_URC_... */
};

ATConn*	ATOpen(const char*);
void	ATClose(ATConn*);

int	ATFildes(ATConn*);
/* ATFildes returns the file descriptor of the underlying serial connection. */

int	ATError(ATConn*, const char** err);
/* ATError returns the errno of the last error. If err is not NULL and this was an internal error,
 * a pointer to a static string describing the error is stored there. */

void	ATCmdSimple(struct ATCmd*, const char *name, const char *args);
int	ATReady(ATConn*);
/* ATReady returns true if the connection is ready to accept a command. */

int	ATSendCmd(ATConn*, struct ATCmd*);
int	ATArgs(struct ATLine *l, const char *fmt, ...);

int	ATFailed(struct ATLine*);
void	ATFree(struct ATLine*);
struct ATLine*	ATNext(ATConn*);
int	ATPending(ATConn*);
struct ATLine*	ATWaitNext(ATConn*);
