#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <poll.h>
#include <signal.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>

#include "at.h"

#define SIZE(X) (sizeof(X)/sizeof((X)[0]))

static void	flush(ATConn*);
static struct ATLine*	parse(ATConn*);
static int	parseurcex(ATConn*, struct ATLine*);
static int	parseurcfixed(ATConn*, struct ATLine*);
static int	pending(ATConn*);

char*	str_append(char *dst, const char *src, size_t capacity);
char*	str_copy(char *dst, const char *src, size_t capacity);

static void die(const char *fmt, ...);

/* used by parseurcfixed */
static struct urcfixed {
	const char *txt;
	int urc;
	int endata;	/* 1 if this can be a final response to the ATA command */
	int endatd;	/* 1 if this can be a final response to the ATD command */
} fixedurcs[] = {
	{"BUSY",	AT_URC_BUSY,	0,	1},
	{"NO ANSWER",	AT_URC_NO_ANSWER,	0,	0},
	{"NO CARRIER",	AT_URC_NO_CARRIER,	1,	1},
	{"NO DIALTONE",	AT_URC_NO_DIALTONE,	0,	1},
	{"RING",	AT_URC_RING,	0,	0},
};

ATConn*
ATOpen(const char *path)
{
	ATConn *conn;
	struct termios at;
	int error = 0;
	int fd = open(path, O_RDWR | O_NONBLOCK);
	if (fd < 0)
		return NULL;
	if (tcgetattr(fd, &at))
		goto FAIL;
	at.c_iflag &= ~ICRNL;
	at.c_lflag &= ~ECHO;
	if (tcsetattr(fd, TCSANOW, &at))
		goto FAIL;
	
	conn = calloc(1, sizeof(*conn));
	if (!conn)
		goto FAIL;
	conn->fd = fd;
	return conn;
FAIL:
	error = errno;
	close(fd);
	errno = error;
	return NULL;
}

void
ATClose(ATConn *c)
{
	if (!c->error)
		close(c->fd);
	free(c);
}

int
ATArgs(struct ATLine *l, const char *fmt, ...)
{
	va_list ap;
	char **str;
	int *num;
	struct ATArg *arg = l->args;
	int nargs = 0;
	va_start(ap, fmt);
	while (*fmt) {
		if (arg->type == AT_ARG_END)
			return nargs;
		switch (*fmt) {
		case 's':
			if (arg->type != AT_ARG_STR)
				return nargs;
			if ((str = va_arg(ap, char**)))
				*str = arg->str;
			break;
		case 'n':
			if (arg->type != AT_ARG_NUM)
				return nargs;
			if ((num = va_arg(ap, int*)))
				*num = arg->num;
			break;
		case '-':
			break;
		default:
			return nargs;
		}
		fmt++;
		arg++;
		nargs++;
	}
	va_end(ap);
	return nargs;
}

void
ATCmdSimple(struct ATCmd *cmd, const char *name, const char *args)
{
	str_copy(cmd->cmd, name, sizeof(cmd->cmd));
	str_copy(cmd->args, args, sizeof(cmd->args));
}
		
int
ATError(ATConn *c, const char **err)
{
	if (err)
		*err = c->errstr;
	return c->error;
}

int
ATFailed(struct ATLine *l)
{
	return l->type == AT_LINE_ERROR || l->type == AT_LINE_INVALID;
}

int
ATFildes(ATConn *c)
{
	return c->fd;
}

void
ATFree(struct ATLine *l)
{
	struct ATArg *a;
	if (!l)
		return;
	free(l->raw);
	if (l->nargs > 0)
		for (a = l->args; a->type != AT_ARG_END; a++)
			if (a->type == AT_ARG_STR)
				free(a->str);
	free(l);
}

struct ATLine*
ATNext(ATConn *c)
{
	struct ATLine *l;
	if (c->error || !ATPending(c))
		return NULL;
	l = parse(c);
	memset(c->line, 0, sizeof(c->line));
	return l;
}

int
ATPending(ATConn *c)
{
	if (!pending(c))
		flush(c);
	return pending(c);
}

int
ATReady(ATConn *c)
{
	return c && !c->outlen && !c->cmd[0];
}

int
ATSendCmd(ATConn *c, struct ATCmd *cmd)
{
	if (c->outlen)
		return 1;
	if (strlen(cmd->cmd) + strlen(cmd->args) + 2 > SIZE(c->out))
		return 1;
	str_copy(c->cmd, cmd->cmd, sizeof(c->cmd));
	c->outlen = snprintf(c->out, SIZE(c->out), "AT%s%s\r", cmd->cmd, cmd->args);
	flush(c);
	return 0;
}

struct ATLine*
ATWaitNext(ATConn *c)
{
	struct pollfd fds = {c->fd, POLLIN};
	while (!ATPending(c) && !c->error)
		if (poll(&fds, 1, -1) < 0)
			return NULL;
	return ATNext(c);
}

void
endcmd(ATConn *c)
{
	if (!c->cmdrunning)
		die("internal error: endcmd() called but no command is being executed\n");
	memset(c->cmd, 0, sizeof(c->cmd));
	memset(c->out, 0, sizeof(c->out));
	c->outlen = c->outpos = c->outechoed = 0;
	c->cmdrunning = 0;
}

void
flush(ATConn *c)
{
	int n, towrite = c->outlen - c->outpos;
	if (c->error)
		return;
	if (c->outlen && towrite > 0) {
		n = write(c->fd, c->out + c->outpos, towrite);
		if (n < towrite && errno != EAGAIN) {
			c->error = errno;
			return;
		}
		c->outpos += n;
	}
	
	if (pending(c))
		return;
	if (c->inplen == SIZE(c->inp)) {
		c->error = EMSGSIZE;
		return;
	}
	
	n = read(c->fd, c->inp+c->inplen, SIZE(c->inp) - c->inplen);
	if (n == 0) {
		c->error = EPIPE;
		c->errstr = "disconnected";
		return;
	} else if (n < 0) {
		if (errno != EAGAIN) {
			c->error = errno;
		}
		return;
	}
	c->inplen += n;
	c->inp[c->inplen] = 0;
}

struct ATLine*
parse(ATConn *c)
{
	struct ATLine *l = calloc(1, sizeof(*l));
	l->args[0].type = AT_ARG_END;
	if (!l) {
		c->error = ENOMEM;
		return NULL;
	}
	
	assert(sizeof(l->cmd) == sizeof(c->cmd));
	memmove(l->cmd, c->cmd, sizeof(c->cmd));
	l->raw = strdup(c->line);
	if (!l->raw) {
		free(l);
		c->error = ENOMEM;
		return NULL;
	}

	if (!c->line[0]) {
		l->type = AT_LINE_NONE;
		return l;
	}
	
	if (parseurcfixed(c, l))
		return l;
	if (parseurcex(c, l))
		return l;
	
	if (!c->cmdrunning) {
		l->type = AT_LINE_URC;
		l->urc = AT_URC_UNKNOWN;
		return l;
	}
	
	if (strcmp(c->line, "OK") == 0) {
		l->type = AT_LINE_OK;
		endcmd(c);
		return l;
	}
	
	if (strcmp(c->line, "ERROR") == 0) {
		l->type = AT_LINE_ERROR;
		l->errtype = AT_ERR_ERROR;
		endcmd(c);
		return l;
	}
	
	l->type = AT_LINE_INVALID;
	return l;
}

static char*
parseargstr(struct ATArg *arg, char *str)
{
	char *end;
	if (str[0] != '"')
		return NULL;
	str++;
	end = strchr(str, '"');
	if (!end)
		return NULL;
	arg->type = AT_ARG_STR;
	arg->str = strndup(str, end - str);
	return end + 1;
}

static char*
parseargnum(struct ATArg *arg, char *str)
{
	arg->type = AT_ARG_NUM;
	arg->num = strtol(str, &str, 10);
	return str;
}
	
int
parseurcex(ATConn *c, struct ATLine *l)
{
	char *line = c->line;
	char *sep, *arg;
	int argn, fail;
	if (line[0] != '+' && line[0] != '^')
		return 0;
	sep = strchr(line, ':');
	if (!sep || sep[1] != ' ')
		return 0;
	str_copy(l->cmd, line, sep - line);
	arg = sep + 2;
	argn = 0;
	for (;;) {
		if (arg[0] == '"')
			arg = parseargstr(&l->args[argn], arg);
		else if (arg[0] >= '0' && arg[0] <= '9')
			arg = parseargnum(&l->args[argn], arg);
		else
			arg = NULL;
		
		if (!arg) {
			l->type = AT_LINE_INVALID;
			return 1;
		}
		argn++;
		
		if (!arg[0])
			break;
		if (arg[0] != ',') {
			l->type = AT_LINE_INVALID;
			return 1;
		}
		arg++;
	}
	l->nargs = argn;
	l->args[argn].type = AT_ARG_END;
	if (c->cmdrunning && strcmp(l->cmd, c->cmd) == 0) {
		l->type = AT_LINE_RESP;
	} else {
		l->type = AT_LINE_URC;
		l->urc = AT_URC_EXTENDED;
	}
	return 1;
}
	
int
parseurcfixed(ATConn *c, struct ATLine *l)
{
	int i;
	int urc = AT_URC_NONE;
	int endata = 0, endatd = 0;
	struct urcfixed *u = NULL;
	for (i = 0; i < SIZE(fixedurcs); i++) {
		if (strcmp(c->line, fixedurcs[i].txt) == 0) {
			u = &fixedurcs[i];
			break;
		}
	}
	if (!u)
		return 0;
	if (c->cmdrunning)
		if (	(strcmp(c->cmd, "D") == 0 && u->endatd) ||
			(strcmp(c->cmd, "A") == 0 && u->endata))
				l->final = 1;
	if (l->final)
		endcmd(c);
	l->type = AT_LINE_URC;
	l->urc = u->urc;
	return 1;
}

/* pending checks if there's an input line waiting to be parsed, possibly fetching one from the input buffer. */
int
pending(ATConn *c)
{
	int i, len;
	char *where;
	if (c->line[0])
		return 1;
	if (c->error)
		return 0;
	if (c->inplen < 2)
		return 0;
	while ((where = strchr(c->inp, '\n'))) {
		*where = 0;
		len = where - c->inp;
		memmove(c->line, c->inp, len + 1);
		for (i = 0; c->inp[i]; i++)
			if (c->inp[i] == '\r')
				c->inp[i] = '%';
		printf("%s$\n", c->inp);
		c->inplen -= len + 1;
		memmove(c->inp, where+1, c->inplen);
		
		if (len == 0)
			continue;
		if (c->line[len-1] == '\r') {
			c->line[len-1] = 0;
			len--;
		}
		if (len == 0)
			continue;
		
		/* If the input line is a prefix of the last sent command line, discard it. */
		/* If the entire command line has been echoed, treat further input as a response. */
		if (c->outpos >= len && strncmp(c->out, c->line, c->outpos) == 0) {
			c->outechoed += len;
			if (c->outechoed == c->outlen)
				c->cmdrunning = 1;
			memset(c->line, 0, sizeof(c->line));
			continue;
		}
		return 1;
	}
	return 0;
}

char*
str_append(char *dst, const char *src, size_t capacity)
{
	size_t dstlen, slen, remaining;
	dstlen = strnlen(dst, capacity);
	if (dstlen == capacity)
		die("str_append: destination buffer unterminated\n");
	slen = strlen(src);
	remaining = capacity - dstlen - 1;
	if (slen > remaining)
		slen = remaining;
	memmove(dst + dstlen, src, slen);
	dst[dstlen + slen] = 0;
	return dst;
}

char*
str_copy(char *dst, const char *src, size_t capacity)
{
	size_t sl = strlen(src);
	if (sl + 1 > capacity)
		sl = capacity - 1;
	memmove(dst, src, sl);
	dst[sl] = 0;
	return dst;
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	abort();
}
