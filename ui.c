#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xdbe.h>
#include <X11/extensions/XInput.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <X11/Xft/Xft.h>

#include <cairo.h>
#include <cairo-xlib.h>
#include <cairo-ft.h>

#include "ui.h"

#define SIZE(X) (sizeof(X)/sizeof((X)[0]))

cairo_font_face_t*	loadfont(const char *fcname);

void button_draw(struct UIElem *ui);
void button_press(struct UIElem *ui, double x, double y, int action);

void label_draw(struct UIElem *ui);

int	xbutton(int x, int y, int down, unsigned int btn);
int	xkey(KeySym ksym, int kdown);
int	xmotion(int x, int y, unsigned int btn);
void xresize(void);

Display *disp;
Window root, win;
GC gc;
int screen;

cairo_surface_t *surf;
cairo_t *ctx;
Pixmap surfp;

int winw, winh;

int uielemslen;
struct UIElem uielems[256];
struct UIElem *last_clicked;

void
button_init(struct UIElem *e, struct Btn *b)
{
	memset(e, 0, sizeof(*e));
	e->data = b;
	e->press = button_press;
	e->motion = NULL;
	e->draw = button_draw;
	e->dirty = 1;
}

cairo_pattern_t*
mkstripes(double spacing, double width, double color[3])
{
	cairo_pattern_t *p;
	cairo_surface_t *surf = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, spacing, spacing);
	cairo_t *c = cairo_create(surf);
	
	cairo_set_source_vec3(c, color);
	cairo_set_line_width(c, width);
	cairo_move_to(c, -width, 0.5*spacing + width);
	cairo_line_to(c, 0.5*spacing + width, -width);
	cairo_stroke(c);
	cairo_move_to(c, -width, 1.5*spacing + width);
	cairo_line_to(c, 1.5*spacing + width, -width);
	cairo_stroke(c);
	cairo_destroy(c);
	
	p = cairo_pattern_create_for_surface(surf);
	cairo_surface_destroy(surf);
	cairo_pattern_set_extend(p, CAIRO_EXTEND_REPEAT);
	return p;
}

void
button_draw(struct UIElem *ui)
{
	struct Btn *btn = ui->data;
	struct BtnStyle *style = btn->clicked ? btn->style_flash : btn->style_normal;
	double w = ui->w, h = ui->h;
	double btnh, subh;
	if (btn->toggle) {
		btnh = (int) h*0.7 - 4;
		subh = (int) h*0.3;
	} else {
		btnh = h;
		subh = (int) h*0.35;
	}
	
	cairo_set_font_face(ctx, *(style->font));
	
	/* draw the border */
	cairo_set_source_vec3(ctx, style->border);
	cairo_rectangle(ctx, 0, 0, w, btnh);
	cairo_fill(ctx);
	
	cairo_save(ctx);
	cairo_rectangle(ctx, 2, 2, w-4, btnh-4);
	cairo_clip(ctx);
	
	/* draw the button background */
	cairo_set_source_vec3(ctx, style->bg);
	cairo_rectangle(ctx, 0, 0, w, btnh);
	cairo_fill(ctx);
	
	/* draw the button label */
	cairo_move_to(ctx, w/2, btn->toggle ? btnh / 2 : btnh / 3);
	cairo_set_source_vec3(ctx, style->txt);
	cairo_set_font_size(ctx, 25);
	textcenter("%s", btn->label);
	
	if (!btn->toggle && btn->sublabel) {
		cairo_set_source_vec3(ctx, style->subbg);
		cairo_rectangle(ctx, 0, btnh - subh, w, subh);
		cairo_fill(ctx);
		
		cairo_move_to(ctx, w/2, btnh - subh/2 - 2);
		cairo_set_source_vec3(ctx, style->subtxt);
		cairo_set_font_size(ctx, 16);
		textcenter("%s", btn->sublabel);
	}
	
	cairo_restore(ctx);		/* restore clip region */
	if (!btn->toggle)
		return;
	
	if (btn->clicked)
		cairo_set_source_vec3(ctx, btn->style_flash->subbg);
	else
		cairo_set_source_vec3(ctx, btn->on ? btn->switch_color_on : btn->switch_color_off);
	cairo_rectangle(ctx, 0, h - subh, w, subh);
	cairo_fill(ctx);
	
	if (btn->on && btn->sublabel) {
		cairo_move_to(ctx, w/2, h - subh/2);
		cairo_set_source_rgb(ctx, 0, 0, 0);
		cairo_set_font_size(ctx, 16);
		textcenter("%s", btn->sublabel);
	}
}

void
button_press(struct UIElem *ui, double x, double y, int action)
{
	struct Btn *btn = ui->data;
	ui->dirty = 1;
	switch (action) {
	case DOWN:
		btn->clicked = 1;
		break;
	case UP:
		btn->clicked = 0;
		if (btn->toggle)
			btn->on = !btn->on;
		if (btn->action)
			btn->action(btn);
		break;
	case UP_2:
		btn->clicked = 0;
		if (btn->action2)
			btn->action2(btn);
		break;
	case CANCEL:
		btn->clicked = 0;
	}
}

void
label_init(struct UIElem *e, struct Label *l)
{
	memset(e, 0, sizeof(*e));
	e->data = l;
	e->draw = label_draw;
	e->dirty = 1;
}

void
label_draw(struct UIElem *ui)
{
	struct Label *l = ui->data;
	cairo_set_font_face(ctx, *(l->font));
	cairo_set_font_size(ctx, l->size);
	cairo_set_source_vec3(ctx, l->color);
	cairo_move_to(ctx, 0, 0);
	textcenter("%s", l->txt);
}

void
ui_clear(void)
{
	memset(uielems, 0, sizeof(uielems));
	uielemslen = 0;
}	

struct UIElem*
ui_locate(double x, double y)
{
	struct UIElem *e;
	int i;
	for (i = 0; i < uielemslen; i++) {
		e = &uielems[i];
		if (x < e->x || x > e->x + e->w || y < e->y || y > e->y + e->h)
			continue;
		return e;
	}
	return NULL;
}

cairo_font_face_t*
loadfont(const char *fcname)
{
	FcPattern *pt;
	FcConfig *conf;
	FcBool ok;
	cairo_font_face_t *font;
	cairo_status_t err;
	
	conf = FcInitLoadConfigAndFonts();
	pt = FcNameParse((FcChar8 *) fcname);
	if (!pt) {
		fprintf(stderr, "WARNING: failed to parse font pattern \"%s\"", fcname);
		return NULL;
	}
	
	ok = FcConfigSubstitute(conf, pt, FcMatchPattern);
	if (ok == FcFalse) {
		fprintf(stderr, "WARNING: failed to perform font substitution for font \"%s\"", fcname);
		return NULL;
	}
	
	font = cairo_ft_font_face_create_for_pattern(pt);
	err = cairo_font_face_status(font);
	if (err != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "WARNING: failed to create font: %s", cairo_status_to_string(err));
		return NULL;
	}
	return font;
}

int
loadfonts(struct FontNames *names)
{
	int i, err = 0;
	cairo_font_face_t *f, *def = NULL;
	struct FontNames *font;
	for (font = names; font->font; font++) {
		f = loadfont(font->name);
		if (!f) {
			if (!def)
				def = loadfont("Arial");
			if (!def)
				return 1;
			f = def;
		}
		*(font->font) = f;
	}
	return 0;
}

int
xbutton(int x, int y, int down, unsigned int btn)
{
	struct UIElem *loc = ui_locate(x, y);
	int action;
	if (down) {
		if (!loc)
			return 0;
		if (loc->press)
			loc->press(loc, x - loc->x, y - loc->y, DOWN);
		last_clicked = loc;
		return 0;
	}
	 
	if (!last_clicked || !last_clicked->press)
		return 0;
	if (loc == last_clicked)
		action = UP;
	else
		action = CANCEL;
	last_clicked->press(last_clicked, x, y, action);
	last_clicked = NULL;
	return 0;
}

int
xevents(void)
{
	XEvent ev;
	int k;
	KeySym sym;
	int changed = 0, force = 0;
	int en = XPending(disp);
	for (int i = 0; i < en; i++) {
		XNextEvent(disp, &ev);
		switch (ev.type) {
		case ConfigureNotify:
			if (winw == ev.xconfigure.width && winh == ev.xconfigure.height)
				break;
			winw = ev.xconfigure.width;
			winh = ev.xconfigure.height;
			xresize();
			force = 1;
			break;
		case KeyPress:
		case KeyRelease:
			sym = XkbKeycodeToKeysym(disp, ev.xkey.keycode, 0, 0);
			if (xkey(sym, ev.type == KeyPress))
				return 1;
			break;
		case ButtonPress:
		case ButtonRelease:
			if (xbutton(ev.xbutton.x, ev.xbutton.y, (ev.xbutton.type == ButtonPress), ev.xbutton.button))
				return 1;
			break;
		case MotionNotify:
			if (xmotion(ev.xmotion.x, ev.xmotion.y, ev.xmotion.state))
				return 1;
			break;
		case Expose:
			force = 1;
			break;
		default:
			continue;
		}
		changed = 1;
	}
	if (changed)
		redraw(force);
	return 0;
}

int
xinit(void)
{
	int x, y, w, h;
	winw = 800;
	winh = 600;
	if (!(disp = XOpenDisplay(NULL)))
		return 1;
	XSynchronize(disp, True);
	screen = DefaultScreen(disp);
	root = RootWindow(disp, screen);
	
	XSetWindowAttributes attr;
	attr.event_mask = StructureNotifyMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | ExposureMask;
	win = XCreateWindow(disp, root, 0, 0, winw, winh, 0, DefaultDepth(disp, screen), CopyFromParent, DefaultVisual(disp, screen), CWEventMask, &attr);
	gc = XCreateGC(disp, win, 0, NULL);
	XSetGraphicsExposures(disp, gc, True);

	surfp = XCreatePixmap(disp, root, winw, winh, DefaultDepth(disp, screen));
	surf = cairo_xlib_surface_create(disp, surfp, DefaultVisual(disp, screen), winw, winh);
	ctx = cairo_create(surf);
	XMapWindow(disp, win);
	uiresize(winw, winh);
	redraw(1);
	return 0;
}

int
xmotion(int x, int y, unsigned int btn)
{
	return 0;
}

void
xresize(void)
{
	cairo_destroy(ctx);
	cairo_surface_destroy(surf);
	XFreePixmap(disp, surfp);
	
	surfp = XCreatePixmap(disp, root, winw, winh, DefaultDepth(disp, screen));
	surf = cairo_xlib_surface_create(disp, surfp, DefaultVisual(disp, screen), winw, winh);
	ctx = cairo_create(surf);
	
	uiresize(winw, winh);
}

void
cairo_set_source_vec3(cairo_t *ctx, double c[3])
{
	cairo_set_source_rgb(ctx, c[0], c[1], c[2]);
}

void
copyvec3(double a[3], double b[3])
{
	a[0] = b[0];
	a[1] = b[1];
	a[2] = b[2];
}

void
setvec3(double v[3], double a, double b, double c)
{
	v[0] = a;
	v[1] = b;
	v[2] = c;
}

void
textcenter(const char *fmt, ...)
{
	static char buf[4096];
	va_list ap;
	double x, y;
	cairo_text_extents_t e;

	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);
	
	cairo_text_extents(ctx, buf, &e);
	x = -e.x_bearing - e.width/2;
	y = -e.y_bearing - e.height/2;
	cairo_rel_move_to(ctx, x, y);
	cairo_show_text(ctx, buf);
}

