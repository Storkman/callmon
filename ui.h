enum {
	DOWN, 	/* button down */
	UP, 	/* button up */
	UP_2, 	/* secondary action - button 2 or long press */
	CANCEL, 	/* click cancelled - don't execute action */
};

struct FontNames {
	cairo_font_face_t **font;
	const char *name;
};

struct Keyarg {
	int i;
	double d;
	void *v;
};

struct Keybind {
	KeySym key;
	int (*func)(struct Keyarg);
	struct Keyarg arg;
};

struct UIElem {
	double x, y, w, h;
	double bg[3];
	void *data;
	void (*press)(struct UIElem *ui, double x, double y, int action);
	void (*motion)(struct UIElem *ui, double x, double y, unsigned int btn);
	void (*draw)(struct UIElem *ui);
	
	int dirty;
};

struct BtnStyle {
	cairo_font_face_t **font;
	double border[3];
	double bg[3];
	double txt[3];
	double subbg[3];
	double subtxt[3];
};

struct Btn {
	const char *label, *sublabel;
	void (*action)(struct Btn *btn);
	void (*action2)(struct Btn *btn);
	void *data;
	struct BtnStyle *style_normal, *style_flash;
	double switch_color_on[3], switch_color_off[3];
	int toggle, on;
	int clicked;
};

struct Label {
	cairo_font_face_t **font;
	double size;
	double color[3];
	char txt[512];
};

void uiresize(int w, int h);
void redraw(int all);

void button_init(struct UIElem *e, struct Btn *b);
void label_init(struct UIElem *e, struct Label *l);

void ui_clear(void);
struct UIElem* ui_locate(double x, double y);

int	loadfonts(struct FontNames *names);
int	xevents(void);
int	xinit(void);

void	cairo_set_source_vec3(cairo_t *ctx, double c[3]);
void	copyvec3(double a[3], double b[3]);
cairo_pattern_t*	mkstripes(double spacing, double width, double color[3]);
void	setvec3(double v[3], double, double, double);
void	textcenter(const char *fmt, ...);

extern Display *disp;

extern cairo_t *ctx;
extern Pixmap surfp;
extern GC gc;

extern Window win;
extern int winw, winh;

extern int uielemslen;
extern struct UIElem uielems[256];
extern struct UIElem *last_clicked;
