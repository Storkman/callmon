#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/select.h>
#include <sys/wait.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xdbe.h>
#include <X11/extensions/XInput.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <X11/Xft/Xft.h>

#include <cairo.h>
#include <cairo-xlib.h>
#include <cairo-ft.h>

#include "ui.h"
#include "at.h"

#define SIZE(X) (sizeof(X)/sizeof((X)[0]))

enum {
	MODE_STANDBY,
	MODE_OFFLINE,
	MODE_RINGING,
	MODE_CALLING,
	MODE_INCALL,
};

typedef void CmdHandler(
	int nlines,
	struct ATLine **lines,
	struct ATLine *final);

struct Cmd {
	struct Cmd *next;
	struct ATCmd c;
	CmdHandler *handler;
};

void	atd(ATConn*, const char*);
int	aterror(void);
void	atevent(void);
void	configcall(void);
void	btn_ans(struct Btn*);
void	btn_dial(struct Btn*);
void	btn_hold(struct Btn*);
void	btn_mute(struct Btn*);
void	btn_quit(struct Btn*);
void	btn_reconnect(struct Btn*);
void	btn_reject(struct Btn*);
void	btn_silence(struct Btn*);
void	cmd_finish(struct ATLine *final);
void	cmd_queue(struct ATCmd *cmd, CmdHandler *handler);
void	cmd_resp(struct ATLine *r);
int	cmd_update(void);
void	handle_clcc(int len, struct ATLine **lines, struct ATLine *final);
void	handle_error(int len, struct ATLine **lines, struct ATLine *final);
void	handle_hangup(int len, struct ATLine **lines, struct ATLine *final);
void	handle_pollcalls(int len, struct ATLine **lines, struct ATLine *final);
void	mute(int);
void	pollcalls(void);
void	reconnect(void);
void	ringstart(void);
void	ringstop(void);
void	setmode(int);
void	sigchld(int);
int	spawn(const char *arg);

static void die(const char *fmt, ...);

static char dialingnum[256], callernum[256];
static int mic_muted;
static int mode = MODE_STANDBY;
static ATConn *modem;

static pid_t ringpid;

static struct Cmd *cmdqueue, *cmdlast;
static struct ATLine **cmdresp;
static int cmdsent;
static int cmdrespn, cmdrespcap;

#include "config.h"

const char *mode_labels[] = {
	[MODE_STANDBY] 	=	"STANDBY",
	[MODE_OFFLINE] 	=	"OFFLINE",
	[MODE_RINGING] 	=	"INCOMING CALL",
	[MODE_CALLING] 	=	"CALLING",
	[MODE_INCALL] 	= 	"AUDIO ONLY",
};

struct Btn panel_standby[] = {
	{"CLOSE", .action = btn_quit},
	{"MUTE MIC", "muted", .action = btn_mute, .toggle = 1},
	{"DIALER", .action = btn_dial},
	{NULL},
};

struct Btn panel_offline[] = {
	{"CLOSE", .action = btn_quit},
	{"RETRY", .action = btn_reconnect},
	{NULL},
};

struct Btn panel_ringing[] = {
	{"DISMISS", .action = btn_quit},
	{"MUTE MIC", "muted", .action = btn_mute, .toggle = 1},
	{"SILENCE", "REJECT", .action = btn_silence, .action2 = btn_reject},
	{"REJECT", .action = btn_reject},
	{"ACCEPT", "", .action = btn_ans, .style_normal = &style_confirm},
	{NULL},
};

struct Btn panel_calling[] = {
	{"HANG UP", "", .action = btn_reject, .style_normal = &style_warning},
	{"MUTE MIC", "muted", .action = btn_mute, .toggle = 1},
	{NULL},
};

struct Btn panel_incall[] = {
	{"HANG UP", "", .action = btn_reject, .style_normal = &style_warning},
	{"MUTE MIC", "muted", .action = btn_mute, .toggle = 1},
	{"HOLD", "TRANSFER", .action = btn_hold},
	{NULL},
};

struct Label status_label = { &font_big, 40, {1, 1, 1} };
struct Label calling_label = { &font_info, 30, {1, 1, 1} };

int
main(int argc, char *argv[])
{
	fd_set fds;
	int xfd, mfd = -1, maxfd, i, n = 0;
	struct timeval zerotime = {0,}, polltime;
	struct timespec now, lastpoll = {0,};
	struct ATCmd cmd;
	
	char **arg = argv+1;
	while (*arg) {
		if (strcmp(*arg, "-c") == 0)
			strncpy(dialingnum, *++arg, sizeof(dialingnum));
		else
			die("unknown option '%s'\n", *arg);
			
		if (!*arg)
			break;
		arg++;
	}
	
	signal(SIGCHLD, sigchld);
	
	if (xinit())
		die("couldn't open display\n");
	if (loadfonts(fontnames))
		die("couldn't load fonts\n");
	reconnect();
	if (!modem) {
		perror("Couldn't access the modem");
	} else {
		pollcalls();
		ATCmdSimple(&cmd, "+COLP", "=0");
		cmd_queue(&cmd, handle_error);
	}
	
	xfd = ConnectionNumber(disp);
	for (;;) {
		FD_ZERO(&fds);
		FD_SET(xfd, &fds);
		if (modem) {
			mfd = ATFildes(modem);
			FD_SET(mfd, &fds);
		} else {
			mfd = -1;
		}
		maxfd = xfd > mfd ? xfd : mfd;
		
		if (XPending(disp)) {
			i = select(maxfd+1, &fds, NULL, NULL, &zerotime);
		} else {
			polltime.tv_sec = 1;
			polltime.tv_usec = 0;
			i = select(maxfd+1, &fds, NULL, NULL, &polltime);
		}
		if (i < 0 && errno != EINTR)
			die("select failed:");
		
		if (XPending(disp) || FD_ISSET(xfd, &fds)) {
			if (xevents())
				return 1;
		}
		
		if (!modem)
			continue;
		cmd_update();
		if (ATPending(modem))
			atevent();
		if (mode == MODE_CALLING) {
			if (clock_gettime(CLOCK_MONOTONIC, &now))
				die("couldn't read the real-time clock:");
			if ((now.tv_sec - lastpoll.tv_sec) * 1000 + (now.tv_nsec - lastpoll.tv_nsec) / 1000000 > call_poll_interval) {
				pollcalls();
				clock_gettime(CLOCK_MONOTONIC, &lastpoll);
			}
		}
	}
	return 0;
}

void
atd(ATConn *c, const char *n)
{
	struct ATCmd cmd;
	char buf[256];
	snprintf(buf, SIZE(buf), "%s;", n);
	ATCmdSimple(&cmd, "D", buf);
	cmd_queue(&cmd, handle_pollcalls);
}

int
aterror(void)
{
	const char *errstr;
	int err;
	if (!modem)
		return 0;
	if (!(err = ATError(modem, &errstr)))
		return 0;
	fprintf(stderr, "Modem error: %s\n", errstr ? errstr : strerror(err));
	ATClose(modem);
	modem = NULL;
	return 1;
}

enum {
	CALL_ACTIVE,
	CALL_HELD,
	CALL_DIALING,
	CALL_ALERTING,
	CALL_INCOMING,
	CALL_WAITING,
};

enum {CALL_VOICE, CALL_DATA, CALL_FAX};

void
handle_clcc(int len, struct ATLine **lines, struct ATLine *final)
{
	int idx, dir, stat, serv, mpty, type;
	int nargs;
	char *num;
	if (final->type != AT_LINE_OK)
		return;
	for (struct ATLine **call = lines; *call; call++) {
		if (ATArgs(*call, "nnnnnsn", &idx, &dir, &stat, &serv, &mpty, &num, &type) != 7) {
			fprintf(stderr, "warning: invalid +CLCC response from modem\n");
			continue;
		}
		if (serv != CALL_VOICE)
			continue;
		switch (stat) {
		case CALL_DIALING:
		case CALL_ALERTING:
			setmode(MODE_CALLING);
			return;
		case CALL_ACTIVE:
			setmode(MODE_INCALL);
			return;
		case CALL_INCOMING:
		case CALL_WAITING:
		case CALL_HELD:
			printf("incoming call from: %s\n", num);
			strncpy(callernum, num, sizeof(callernum));
			setmode(MODE_RINGING);
			return;
		default:
			printf("unknown call state %d\n", stat);
		}
	}
	setmode(MODE_STANDBY);
}

void
handle_error(int len, struct ATLine **lines, struct ATLine *final)
{
	if (ATFailed(final))
		fprintf(stderr, "error executing command %s\n", final->cmd);
}

void
handle_hangup(int len, struct ATLine **lines, struct ATLine *final)
{
	if (ATFailed(final))
		return;
	setmode(MODE_STANDBY);
}

void
handle_pollcalls(int len, struct ATLine **lines, struct ATLine *final)
{
	pollcalls();
}

void
atevent(void)
{
	static struct ATLine **lines;
	static int nlines, caplines;
	
	struct ATLine *ev;
	char *c;
	const char *errstr;
	int err;
	int dir, serv;
	char *num;
	
	if (!lines) {
		caplines = 16;
		lines = malloc(caplines * sizeof(lines[0]));
		if (!lines)
			die("out of memory\n");
	}
	
	while ((ev = ATNext(modem))) {
		switch (ev->type) {
		case AT_LINE_NONE:
			break;
		
		case AT_LINE_OK:
		case AT_LINE_ERROR:
			cmd_finish(ev);
			break;
		
		case AT_LINE_RESP:
			cmd_resp(ev);
			break;
		
		case AT_LINE_URC:
			if (ev->final) {
				cmd_finish(ev);
				break;
			}
			switch (ev->urc) {
			case AT_URC_EXTENDED:
				break;
			case AT_URC_RING:
				if (mode != MODE_RINGING)
					pollcalls();
				break;
			case AT_URC_BUSY:
			case AT_URC_NO_ANSWER:
			case AT_URC_NO_CARRIER:
			case AT_URC_NO_DIALTONE:
				setmode(MODE_STANDBY);
				break;
			}
			break;
		
		case AT_LINE_INVALID:
			fprintf(stderr, "Unrecognized line from the modem: %s\n", ev->raw);
			break;
		}
		ATFree(ev);
	}

	if (aterror())
		setmode(MODE_OFFLINE);
}

void
configcall(void)
{
	struct ATCmd cmd;
	mute(mic_muted);
}

void
mute(int on)
{
	struct ATCmd cmd;
	ATCmdSimple(&cmd, "+CMUT", on ? "=1" : "=0");
	cmd_queue(&cmd, handle_error);
}

void
pollcalls(void)
{
	struct ATCmd cmd;
	ATCmdSimple(&cmd, "+CLCC", "");
	cmd_queue(&cmd, handle_clcc);
}

void
reconnect(void)
{
	if (!modem)
		modem = ATOpen(modem_path);
	setmode(modem ? MODE_STANDBY : MODE_OFFLINE);
}

void
ringstart(void)
{
	ringpid = spawn(ring_cmd);
}

void
ringstop(void)
{
	if (ringpid <= 0)
		return;
	if (kill(ringpid, SIGTERM))
		perror("failed to stop ringing command");
}

void
setmode(int m)
{
	if (m == mode)
		return;
	switch (mode) {
	case MODE_RINGING:
		ringstop();
		break;
	}
	
	switch (m) {
	case MODE_CALLING:
		break;
	case MODE_INCALL:
		configcall();
		break;
	case MODE_RINGING:
		ringstart();
		break;
	default:
		break;
	}
	mode = m;
	uiresize(winw, winh);
	redraw(1);
}

void
sigchld(int sig)
{
	int st;
	pid_t pid;
	while ((pid = waitpid(-1, &st, WNOHANG)) != 0) {
		if (pid < 0) {
			if (errno != ECHILD)
				perror("waitpid failed");
			return;
		} else if (pid == ringpid)
			ringpid = 0;
		else
			fprintf(stderr, "unknown child process terminated: %d\n", pid);
	}
}
	
int
spawn(const char *arg)
{
	int pid;
	
	pid = fork();
	switch (pid) {
	case -1:
		perror("failed to fork");
		return 0;
	case 0:
		execl("/bin/sh", "sh", "-c", arg, NULL);
		die("exec failed:");
	default:
		return pid;
	}
}


void
btn_ans(struct Btn *btn)
{
	struct ATCmd cmd;
	ATCmdSimple(&cmd, "A", "");
	cmd_queue(&cmd, handle_pollcalls);
}

void
btn_dial(struct Btn *btn)
{
	if (!dialingnum[0])
		return;
	atd(modem, dialingnum);
	setmode(MODE_CALLING);
}

void
btn_hold(struct Btn *btn)
{
	struct ATCmd cmd;
	ATCmdSimple(&cmd, "+CHLD", "=2");
	cmd_queue(&cmd, handle_error);
}

void
btn_mute(struct Btn *btn)
{
	mic_muted = btn->on;
	if (mode == MODE_INCALL)
		mute(btn->on);
}

void
btn_quit(struct Btn *btn)
{
	exit(0);
}

void
btn_reconnect(struct Btn *btn)
{
	reconnect();
	uiresize(winw, winh);
	redraw(1);
}

void
btn_reject(struct Btn *btn)
{
	struct ATCmd cmd;
	ATCmdSimple(&cmd, "+QHUP", "=16");
	cmd_queue(&cmd, handle_hangup);
}

void
btn_silence(struct Btn *btn)
{
	ringstop();
}

void
cmd_finish(struct ATLine *final)
{
	struct Cmd *cmd = cmdqueue;
	if (!cmdsent) {
		fprintf(stderr, "final response received, but no command is running\n");
		return;
	}
	cmdresp[cmdrespn] = NULL;
	if (cmd->handler)
		cmd->handler(cmdrespn, cmdresp, final);
	else
		handle_error(cmdrespn, cmdresp, final);
	cmdrespn = 0;
	cmdqueue = cmd->next;
	if (!cmdqueue)
		cmdlast = NULL;
	cmdsent = 0;
	free(cmd);
	cmd_update();
}

void
cmd_queue(struct ATCmd *cmd, CmdHandler *handler)
{
	struct Cmd *c = malloc(sizeof(*c));
	if (!c)
		die("out of memory\n");
	c->c = *cmd;
	c->handler = handler;
	c->next = NULL;
	if (!cmdlast)
		cmdlast = cmdqueue = c;
	else
		cmdlast = cmdlast->next = c;
}

void
cmd_resp(struct ATLine *r)
{
	if (!cmdresp) {
		cmdrespcap = 16;
		cmdresp = malloc(cmdrespcap * sizeof(struct ATLine*));
		if (!cmdresp)
			die("out of memory\n");
	}
	if (cmdrespn == cmdrespcap-1) {
		cmdrespcap *= 2;
		cmdresp = realloc(cmdresp, cmdrespcap * sizeof(struct ATLine*));
		if (!cmdresp)
			die("out of memory\n");
	}
	cmdresp[cmdrespn++] = r;
}

int
cmd_update(void)
{
	if (cmdsent || !cmdqueue || !ATReady(modem))
		return 0;
	if (ATSendCmd(modem, &cmdqueue->c))
		return 1;
	cmdsent = 1;
	return 0;
}

void
redraw(int all)
{
	double amber[3] = {1, 203.0/255, 0};
	double black[3] = {0, 0, 0}, white[3] = {1, 1, 1};
	double *bg;
	
	cairo_pattern_t *stripe;
	double panelx = winw - button_w - button_gap*2;
	struct UIElem *e;
	cairo_region_t *changed = cairo_region_create();
	cairo_rectangle_int_t rect;
	int i, n;
	
	if (all) {
		switch (mode) {
		case MODE_OFFLINE:
			stripe = mkstripes(76, 24, white);
			bg = bgcolor_offline;
			break;
		case MODE_STANDBY:
			stripe = NULL;
			bg = bgcolor;
			break;
		case MODE_RINGING:
			stripe = mkstripes(38, 6, amber);
			bg = bgcolor;
			break;
		case MODE_CALLING:
		case MODE_INCALL:
			stripe = NULL;
			bg = bgcolor;
			break;
		default:
			die("invalid mode\n");
		}
			
		cairo_identity_matrix(ctx);
		cairo_set_source_vec3(ctx, bg);
		cairo_rectangle(ctx, 0, 0, panelx, winh);
		cairo_fill(ctx);
		
		cairo_set_source_rgb(ctx, 0, 0, 0);
		cairo_rectangle(ctx, panelx, 0, winw, winh);
		cairo_fill(ctx);
		
		if (stripe) {
			cairo_set_source(ctx, stripe);
			cairo_rectangle(ctx, 0, 0, 50, winh);
			cairo_fill(ctx);
			cairo_pattern_destroy(stripe);
		}
	}
	
	for (i = 0; i < uielemslen; i++) {
		e = &uielems[i];
		if (!e->draw)
			continue;
		if (!all && !e->dirty)
			continue;
		cairo_identity_matrix(ctx);
		cairo_set_source_vec3(ctx, e->bg);
		cairo_rectangle(ctx, e->x, e->y, e->w, e->h);
		cairo_fill(ctx);
		cairo_translate(ctx, e->x, e->y);
		e->draw(e);
		e->dirty = 0;
		if (all)
			continue;
		rect.x = e->x;
		rect.y = e->y;
		rect.width = ceil(e->w);
		rect.height = ceil(e->h);
		cairo_region_union_rectangle(changed, &rect);
	}
	
	if (all) {
		XCopyArea(disp, surfp, win, gc, 0, 0, winw, winh, 0, 0);
		return;
	}
	
	n = cairo_region_num_rectangles(changed);
	for (i = 0; i < n; i++) {
		cairo_region_get_rectangle(changed, i, &rect);
		XCopyArea(disp, surfp, win, gc, rect.x, rect.y, rect.width, rect.height, rect.x, rect.y);
	}
}

struct UIElem*
newbutton(struct Btn *b)
{
	struct UIElem *e = &uielems[uielemslen++];
	if (!b->style_normal)
		b->style_normal = &style_normal;
	if (!b->style_flash)
		b->style_flash = &style_flash;
	copyvec3(b->switch_color_on, switch_color_on);
	copyvec3(b->switch_color_off, switch_color_off);
	button_init(e, b);
	e->w = button_w;
	e->h = button_h;
	setvec3(e->bg, 0, 0, 0);
	return e;
}

struct UIElem*
newlabel(struct Label *l, const char *fmt, ...)
{
	struct UIElem *e = &uielems[uielemslen++];
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(l->txt, SIZE(l->txt), fmt, ap);
	va_end(ap);
	label_init(e, l);
	return e;
}

void
addpanel(void)
{
	struct Btn *b = NULL;
	struct UIElem *e;
	switch (mode) {
	case MODE_STANDBY:
		b = panel_standby;
		break;
	case MODE_OFFLINE:
		b = panel_offline;
		break;
	case MODE_RINGING:
		b = panel_ringing;
		break;
	case MODE_CALLING:
		b = panel_calling;
		break;
	case MODE_INCALL:
		b = panel_incall;
		break;
	default:
		die("invalid mode");
	}
	e = newbutton(b);
	e->x = winw - button_gap - button_w;
	e->y = button_gap;
	
	for (b++; b->label; b++) {
		e = newbutton(b);
		e->x = winw - button_gap - button_w;
		e->y = (e-1)->y + button_gap + button_h;
	}
}

void
uiresize(int w, int h)
{
	struct UIElem *e;
	last_clicked = NULL;
	ui_clear();
	addpanel();
	
	if (mode >= SIZE(mode_labels) || !mode_labels[mode])
		die("invalid mode");
	e = newlabel(&status_label, "%s", mode_labels[mode]);
	e->x = (w - button_w - button_gap*2) / 2;
	e->y = 125;
	e = NULL;
	switch (mode) {
	case MODE_STANDBY:
	case MODE_CALLING:
		if (dialingnum[0])
			e = newlabel(&calling_label, "[ %s ]", dialingnum);
		break;
	case MODE_INCALL:
		if (callernum[0])
			e = newlabel(&calling_label, "[ %s ]", callernum);
		else if (dialingnum[0])
			e = newlabel(&calling_label, "[ %s ]", dialingnum);
		break;
	case MODE_RINGING:
		if (callernum[0])
			e = newlabel(&calling_label, "From: [ %s ]", callernum);
		break;
	case MODE_OFFLINE:
		break;
	}
	if (e) {
		e->x = (w - button_w - button_gap*2) / 2;
		e->y = (e-1)->y + 100;
	}
}

int
xkey(KeySym ksym, int kdown)
{
	const char *mk;
	for (int i = 0; i < SIZE(keys); i++) {
		if (ksym != keys[i].key)
			continue;
		if (kdown)
			return keys[i].func(keys[i].arg);
		else
			return 0;
	}
	return 0;
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	abort();
}
