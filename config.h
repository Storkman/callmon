cairo_font_face_t *font_label, *font_big, *font_info;

const char *modem_path = "/dev/ttyUSB_at";

const char *ring_cmd = "mpv --loop=inf --vo=null --msg-level=all=error /usr/local/share/callmon/ring.mp3";

struct FontNames fontnames[] = {
	{&font_label, 	"Bebas Kai"},
	{&font_big, 	"D-DIN Exp:style=Bold"},
	{&font_info,	"D-DIN"},
	{NULL,	NULL}
};

int call_poll_interval = 1000;

double button_gap = 20, button_border = 2, button_w = 153, button_h = 77;

double bgcolor[3] = {14.0/255, 25.0/255, 27.0/255};
double bgcolor_offline[3] = {40.0/255, 40.0/255, 40.0/255};

double switch_color_on[3] = {1, 204.0/255, 0};
double switch_color_off[3] = {14.0/255, 25.0/255, 27.0/255};

struct BtnStyle style_normal = {
	.font = &font_label,
	.border = {85/255.0, 153/255.0, 1}, .bg = {0, 0, 0}, .txt = {1, 1, 1},
	.subbg = {30/255.0, 54/255.0, 58/255.0}, .subtxt = {255/255.0, 230/255.0, 128/255.0},
};

struct BtnStyle style_flash = {
	.font = &font_label,
	.border = {1, 1, 1}, .bg = {1, 1, 1}, .txt = {0, 0, 0},
	.subbg = {1, 1, 1}, .subtxt = {0, 0, 0},
};

struct BtnStyle style_confirm = {
	.font = &font_label,
	.border = {85.0/255, 212.0/255, 0}, .bg = {0, 0, 0}, .txt = {85.0/255, 212.0/255, 0},
	.subbg = {11.0/255, 40.0/255, 11.0/255}, .subtxt = {0, 0, 0},
};

struct BtnStyle style_warning = {
	.font = &font_label,
	.border = {255.0/255, 203.0/255, 0}, .bg = {0, 0, 0}, .txt = {255.0/255, 203.0/255, 0},
	.subbg = {85.0/255, 68.0/255, 0.0/255}, .subtxt = {0, 0, 0},
};

struct Keybind keys[] = {
};
